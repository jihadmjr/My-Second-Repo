from django.shortcuts import render

# Create your views here.
response = {}
def index(request):
    response['author'] = 'Muhammad Jihad Rinaldi'
    return render(request, "lab_6/lab_6.html", response)
