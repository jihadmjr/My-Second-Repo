from django.db import models
from django.utils import timezone
from datetime import datetime, date
import pytz

class Message(models.Model):
    def converterTZ():
        return timezone.now() + timezone.timedelta(hours = 7)

    name = models.CharField(max_length=27)
    email = models.EmailField()
    message = models.TextField()
    created_date = models.DateTimeField(default=converterTZ)

    def __str__(self):
        return self.message
