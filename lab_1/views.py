from django.shortcuts import render
from datetime import datetime, date

# Enter your name here
mhs_name = 'Muhammad Jihad Rinaldi' # TODO Implement this
curr_year = int(datetime.now().strftime("%y"))
birth_date = datetime.strptime("98/12/06" , "%y/%m/%d") #TODO Implement this, format (Year, Month, Date)

# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age': calculate_age(birth_date.year)}
    return render(request, 'lab_1/lab_1.html', response)

def calculate_age(birth_year):
    today = date.today()
    return today.year - birth_year
