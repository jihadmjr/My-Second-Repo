from django.shortcuts import render
from lab_1.views import mhs_name, birth_date
#Create a list of biodata that you wanna show on webpage:
#[{'subject' : 'Name', 'value' : 'Igor'},{{'subject' : 'Birth Date', 'value' : '11 August 1970'},{{'subject' : 'Sex', 'value' : 'Male'}
#TODO Implement
bio_dict = [{'subject' : 'Name', 'value' : mhs_name},\
{'subject' : 'Birth Date', 'value' : birth_date.strftime('%d %B %Y')},\
{'subject' : 'Born Place', 'value' : 'Jakarta'},\
{'subject' : 'Sex', 'value' : 'Male'},\
{'subject' : 'Job', 'value' : 'Student'},\
{'subject' : 'Status', 'value' : 'Single'},\

]

def index(request):
    response = {'bio_dict': bio_dict}
    return render(request, 'lab_2_addon/lab_2_addon.html', response)
