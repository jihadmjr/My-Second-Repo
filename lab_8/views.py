from django.shortcuts import render

# Create your views here.
response = {}
def index(request):
    response['author'] = 'Muhammad Jihad Rinaldi'
    return render(request, "lab_8/lab_8.html", response)
